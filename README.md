# The sputum microbiome in pulmonary tuberculosis and its association with disease manifestations: a cross-sectional study

NOTE: The executable jupyter notebook version of this README is available as README.ipynb

Contact person: Monica R. Ticlla <br>
Contact e-mail: mticlla@gmail.com

## 0. Dependencies
---

To re-run the analyses of this project, first install (or ask your system admin) the following software:

- Python >=3.7.6
- Conda >= v4.6.11
- Singularity >= 3.5.3
- SRA Toolkit v2.10.7

## 1. Set up environment

---

### 1.1 Download this repository

```bash
# Run this in a terminal
$ git clone https://git.scicore.unibas.ch/TBRU/tbdarbiome_cases.git
$ cd tbdarbiome_cases
```

### 1.2 Create conda virtual environment

**Create virtual environment for QIIME2 and downstream analyses**

```bash
# Run this in a terminal
$ conda activate tb_microbiome
$ conda env create --file ./config/conda_py37r36.yml
```

**Register environment as kernel for Jupyter notebooks**

```bash
# Run this in a terminal
$ conda activate tb_microbiome
$ python -m ipykernel install --user --name=tb_microbiome
```

**Install R package**

There are two R packages that can't be installed through the conda environment configuration file, they need to be manually installed, as follows:

- Open a terminal
- Activate the conda environment used for this project:

  ```bash
  $ conda activate tb_microbiome
  ```
- Start the R environment:

  ```bash
  $ R
  ```
- Within the R environment, install "memisc":

  ```r
  > install.packages('memisc')
  ```
- Within the R environment, install "Table1":

  ```r
  > devtools::install_github("emwozniak/Table1")
  ```


### 1.3 Download containers

Download the container from SingularityHub (strongly recommended):


```bash
%%bash
mkdir -p ./containers
singularity pull --dir ./containers --name meta16S.sif shub://mticlla/OmicSingularities:meta16s
```

     2.88 GiB / 2.88 GiB  100.00% 59.96 MiB/s 49s


    [34mINFO:   [0m Downloading shub image



```bash
%%bash
singularity inspect --list-apps ./containers/meta16S.sif
```

    QIIME2
    


### 1.4 Install MetagenomicSnake

Follow instructions from https://git.scicore.unibas.ch/TBRU/MetagenomicSnake#step-1-install-workflow.

## 2. Download fastq files

### 2.1 16S-rRNA-gene amplicon (V3-V4) sequencing data

**Locally from sciCORE storage**

Notice that you need to define the variable GROUP in your environment. GROUP is the absolute path to the Gagneux group directory at sciCORE.


```bash
%%bash
mkdir -p ./data/raw/tz_spt_cases
bash -O extglob ./src/data/raw_get_16s_scicore_fastqs.sh -d './data/raw/tz_spt_cases' -g 'tz_spt_cases'
```

    Number of files from run1
    66
    Number of files from run2
    382
    Number of files from run3
    222
    Number of links created in ./data/raw/tz_spt_cases/fastq:
    670
    Number of samples:
    335



```python
!wc -l data/raw/tz_spt_cases/sample_ids_mapping.txt
```

    336 data/raw/tz_spt_cases/sample_ids_mapping.txt


**Remotely from NCBI SRA**

TODO

### 2.2 Whole Metagenome Shotgun Sequencing (WMS-S) data

**Locally from sciCORE storage**


```bash
%%bash --err error
mkdir -p ./data/raw/wms/wms_cases
bash src/data/raw_get_wms_scicore_fastqs.sh \
    -d ./data/raw/wms/wms_cases \
    -s "dbsse" \
    -g "tz_spt_cases"
```

    Nr. of samples for which to retrieve fastq filepaths: 130
    Number of links created in ./data/raw/wms/wms_cases/fastq: 938
    Number of samples: 130



```python
!ls -lh ./data/raw/wms/wms_cases/
```

    total 192K
    drwxr-xr-x 2 ticlla gagneux  64K 17. Jun 17:17 fastq
    -rw-r--r-- 1 ticlla gagneux 2.7K 19. Jun 15:58 sample_ids_mapping.txt
    -rw-r--r-- 1 ticlla gagneux  910 19. Jun 15:58 sample_names.txt



```python
!head ./data/raw/wms/wms_cases/sample_ids_mapping.txt
```

    sample-id	SpecimenID	SubjectID
    M00001	S00181	H00183
    M00002	S00375	H00368
    M00004	NC3	
    M00007	S00388	H00382
    M00011	S00249	H00247
    M00012	S00213	H00212
    M00013	S00491	H00486
    M00016	S00069	H00069
    M00021	S00441	H00437


**Remotely from NCBI SRA**

TODO

## 3. Taxonomic profiling

### 3.1 Initial quality control (QC) and denoising of paired-end 16S rRNA V3-V4 reads with QIIME2 and DADA2


```bash
%%bash
export REPORT_DATE=$(date +%Y-%m-%d)
my_notebook='./notebooks/00_MOT_qiime2-qc-denoising-features_pairedreads.ipynb'
time jupyter nbconvert \
    --ExecutePreprocessor.kernel_name='tb_microbiome' \
    --to html \
    --output-dir='./reports' \
    --output=${REPORT_DATE}_$(echo ${my_notebook} | cut -d'/' -f3) \
    --execute \
    --ExecutePreprocessor.timeout=-1 ${my_notebook}
```

    [NbConvertApp] Converting notebook ./notebooks/0_MOT_qiime2-qc-denoising-features_pairedreads.ipynb to html
    [NbConvertApp] Executing notebook with kernel: tb_microbiome
    [NbConvertApp] Writing 329812 bytes to ./reports/2020-06-11_0_MOT_qiime2-qc-denoising-features_pairedreads.ipynb.html
    
    real	53m32.395s
    user	196m32.272s
    sys	4m56.193s


### 3.2 Taxonomic profiling of 16S rRNA V3-V4 R1 reads with QIIME2 and DADA2


```bash
%%bash
let "CPU_AVAIL = $(nproc) - 1"
echo "Nr of CPUs available: ${CPU_AVAIL}"

export REPORT_DATE=$(date +%Y-%m-%d)
my_notebook='./notebooks/01_MOT_qiime2-qc-dada2_denoising_features_read1.ipynb'

time jupyter nbconvert \
    --ExecutePreprocessor.kernel_name='tb_microbiome' \
    --to html \
    --output-dir='./reports' \
    --output=${REPORT_DATE}_$(echo ${my_notebook} | cut -d'/' -f3) \
    --execute \
    --allow-errors \
    --ExecutePreprocessor.timeout=-1 ${my_notebook}
```

    Nr of CPUs available: 7


    [NbConvertApp] Converting notebook ./notebooks/1_MOT_qiime2-qc-dada2_denoising_features_read1.ipynb to html
    [NbConvertApp] Executing notebook with kernel: tb_microbiome
    [NbConvertApp] Writing 405677 bytes to ./reports/2020-06-12_1_MOT_qiime2-qc-dada2_denoising_features_read1.ipynb.html
    
    real	331m46.524s
    user	1668m12.864s
    sys	22m52.708s



```python
from IPython.display import SVG, display
def show_svg():
    display(SVG('./reports/figures/16S-AS_QC-filtering-denoising.svg'))
show_svg()
```


![svg](README_files/README_29_0.svg)


### 3.3 QC-preprocessing-taxonomic profiling of Whole Metagenome Shotgun Sequencing (WMS-S) data with MetaSnake


```bash
%%bash
let "CPU_AVAIL = $(nproc) - 1"
echo "Nr of CPUs available: ${CPU_AVAIL}"

export REPORT_DATE=$(date +%Y-%m-%d)
my_notebook='./notebooks/02_MOT_metasnk-wms-data-processing.ipynb'

time jupyter nbconvert \
    --ExecutePreprocessor.kernel_name='tb_microbiome' \
    --execute \
    --allow-errors \
    --clear-output \
    --ExecutePreprocessor.timeout=-1 \
    ${my_notebook}

time jupyter nbconvert \
    --ExecutePreprocessor.kernel_name='tb_microbiome' \
    --to html \
    --output-dir='./reports' \
    --output=${REPORT_DATE}_$(echo ${my_notebook} | cut -d'/' -f3) \
    ${my_notebook}
```

    Nr of CPUs available: 7


    [NbConvertApp] WARNING | Config option `template_path` not recognized by `NotebookExporter`.
    [NbConvertApp] Converting notebook ./notebooks/02_MOT_metasnk-wms-data-processing.ipynb to notebook
    [NbConvertApp] Executing notebook with kernel: tb_microbiome
    [NbConvertApp] Writing 754587 bytes to ./notebooks/02_MOT_metasnk-wms-data-processing.ipynb
    
    real	0m37.730s
    user	0m33.792s
    sys	0m6.401s
    [NbConvertApp] Converting notebook ./notebooks/02_MOT_metasnk-wms-data-processing.ipynb to html
    [NbConvertApp] Writing 1048201 bytes to ./reports/2020-07-14_02_MOT_metasnk-wms-data-processing.ipynb.html
    
    real	0m1.991s
    user	0m1.388s
    sys	0m0.329s


Here a summary of the pre-processing of the WMS-S dataset:


```python
from IPython.display import SVG, display
def show_svg():
    display(SVG('./reports/figures/WMS-S_MetaSnk-preQC_counts.svg'))
show_svg()
```


![svg](README_files/README_33_0.svg)


## 4  Identification of potential contaminants and assessment of consistency with mock community


```bash
%%bash
let "CPU_AVAIL = $(nproc) - 1"
echo "Nr of CPUs available: ${CPU_AVAIL}"

export REPORT_DATE=$(date +%Y-%m-%d)
my_notebook='./notebooks/03_MOT_decontam.ipynb'

time jupyter nbconvert \
    --ExecutePreprocessor.kernel_name='tb_microbiome' \
    --execute \
    --allow-errors \
    --clear-output \
    --ExecutePreprocessor.timeout=-1 \
    ${my_notebook}

time jupyter nbconvert \
    --to html \
    --output-dir='./reports' \
    --output=${REPORT_DATE}_$(echo ${my_notebook} | cut -d'/' -f3) \
    ${my_notebook}
```

    Nr of CPUs available: 7


    [NbConvertApp] WARNING | Config option `template_path` not recognized by `NotebookExporter`.
    [NbConvertApp] Converting notebook ./notebooks/03_MOT_decontam.ipynb to notebook
    [NbConvertApp] Executing notebook with kernel: tb_microbiome
    [NbConvertApp] Writing 459565 bytes to ./notebooks/03_MOT_decontam.ipynb
    
    real	1m50.283s
    user	1m15.001s
    sys	0m14.480s
    [NbConvertApp] Converting notebook ./notebooks/03_MOT_decontam.ipynb to html
    [NbConvertApp] Writing 822616 bytes to ./reports/2020-07-14_03_MOT_decontam.ipynb.html
    
    real	0m1.990s
    user	0m1.705s
    sys	0m0.259s


Potential contaminants indetified in the 16S-AS dataset:


```python
from IPython.display import IFrame
IFrame('./reports/figures/16S-AS_decontam_panel.pdf',width=600, height=600)
```





<iframe
    width="600"
    height="600"
    src="./reports/figures/16S-AS_decontam_panel.pdf"
    frameborder="0"
    allowfullscreen
></iframe>




Potential contaminants indetified in the WMS-S dataset:


```python
from IPython.display import IFrame
IFrame('./reports/figures/WMS-S_decontam_panel.pdf',width=600, height=600)
```





<iframe
    width="600"
    height="600"
    src="./reports/figures/WMS-S_decontam_panel.pdf"
    frameborder="0"
    allowfullscreen
></iframe>




## 5. Assess comparibility of sputum samples across categories of TB disease manifestations


```bash
%%bash
let "CPU_AVAIL = $(nproc) - 1"
echo "Nr of CPUs available: ${CPU_AVAIL}"

export REPORT_DATE=$(date +%Y-%m-%d)
my_notebook='./notebooks/04_MOT_comparability-sputum.ipynb'

time jupyter nbconvert \
    --ExecutePreprocessor.kernel_name='tb_microbiome' \
    --execute \
    --allow-errors \
    --clear-output \
    --ExecutePreprocessor.timeout=-1 \
    ${my_notebook}

time jupyter nbconvert \
    --to html \
    --output-dir='./reports' \
    --output=${REPORT_DATE}_$(echo ${my_notebook} | cut -d'/' -f3) \
    ${my_notebook}
```

    Nr of CPUs available: 7


    [NbConvertApp] WARNING | Config option `template_path` not recognized by `NotebookExporter`.
    [NbConvertApp] Converting notebook ./notebooks/04_MOT_comparability-sputum.ipynb to notebook
    [NbConvertApp] Executing notebook with kernel: tb_microbiome
    [NbConvertApp] Writing 101507 bytes to ./notebooks/04_MOT_comparability-sputum.ipynb
    
    real	0m30.654s
    user	0m10.384s
    sys	0m2.266s
    [NbConvertApp] Converting notebook ./notebooks/04_MOT_comparability-sputum.ipynb to html
    [NbConvertApp] Writing 400017 bytes to ./reports/2020-07-15_04_MOT_comparability-sputum.ipynb.html
    
    real	0m7.965s
    user	0m1.817s
    sys	0m0.662s



```python
from IPython.display import SVG, display
def show_svg():
    display(SVG('./reports/figures/sputum_volume_by_disease_manifestations.svg'))
show_svg()
```


![svg](README_files/README_42_0.svg)


## 6. Assessment of detectability of Mycobacterium tubercuclosis by 16S-AS and  WMS-S


```bash
%%bash
let "CPU_AVAIL = $(nproc) - 1"
echo "Nr of CPUs available: ${CPU_AVAIL}"

export REPORT_DATE=$(date +%Y-%m-%d)
my_notebook='./notebooks/05_MOT_Mtb-detection.ipynb'

time jupyter nbconvert \
    --ExecutePreprocessor.kernel_name='tb_microbiome' \
    --execute \
    --allow-errors \
    --clear-output \
    --ExecutePreprocessor.timeout=-1 \
    ${my_notebook}

time jupyter nbconvert \
    --to html \
    --output-dir='./reports' \
    --output=${REPORT_DATE}_$(echo ${my_notebook} | cut -d'/' -f3) \
    ${my_notebook}
```

    Nr of CPUs available: 7


    [NbConvertApp] WARNING | Config option `template_path` not recognized by `NotebookExporter`.
    [NbConvertApp] Converting notebook ./notebooks/05_MOT_Mtb-detection.ipynb to notebook
    [NbConvertApp] Executing notebook with kernel: tb_microbiome
    [NbConvertApp] Writing 85507 bytes to ./notebooks/05_MOT_Mtb-detection.ipynb
    
    real	0m44.009s
    user	0m24.750s
    sys	0m3.475s
    [NbConvertApp] Converting notebook ./notebooks/05_MOT_Mtb-detection.ipynb to html
    [NbConvertApp] Writing 401571 bytes to ./reports/2020-07-15_05_MOT_Mtb-detection.ipynb.html
    
    real	0m9.920s
    user	0m1.948s
    sys	0m0.715s



```python
from IPython.display import SVG, display
def show_svg():
    display(SVG('./reports/figures/mycobacterium_asv_spp_by_afb_grading.svg'))
show_svg()
```


![svg](README_files/README_45_0.svg)


## 7. Compute rarefaction curves for Faith's phylogenetic diversity


```bash
%%bash
let "CPU_AVAIL = $(nproc) - 1"
echo "Nr of CPUs available: ${CPU_AVAIL}"

export REPORT_DATE=$(date +%Y-%m-%d)
my_notebook='./notebooks/06_MOT_alpha-rarefaction.ipynb'

time jupyter nbconvert \
    --ExecutePreprocessor.kernel_name='tb_microbiome' \
    --execute \
    --allow-errors \
    --clear-output \
    --ExecutePreprocessor.timeout=-1 \
    ${my_notebook}

time jupyter nbconvert \
    --to html \
    --output-dir='./reports' \
    --output=${REPORT_DATE}_$(echo ${my_notebook} | cut -d'/' -f3) \
    ${my_notebook}
```

    Nr of CPUs available: 7


    [NbConvertApp] WARNING | Config option `template_path` not recognized by `NotebookExporter`.
    [NbConvertApp] Converting notebook ./notebooks/06_MOT_alpha-rarefaction.ipynb to notebook
    [NbConvertApp] Executing notebook with kernel: tb_microbiome
    [NbConvertApp] Writing 143482 bytes to ./notebooks/06_MOT_alpha-rarefaction.ipynb
    
    real	9m0.002s
    user	8m1.819s
    sys	0m43.882s
    [NbConvertApp] Converting notebook ./notebooks/06_MOT_alpha-rarefaction.ipynb to html
    [NbConvertApp] Writing 413726 bytes to ./reports/2020-07-16_06_MOT_alpha-rarefaction.ipynb.html
    
    real	0m1.739s
    user	0m1.189s
    sys	0m0.244s



```python
from IPython.display import SVG, display
def show_svg():
    display(SVG('./reports/figures/16S_Faith-rarefaction-curves.svg'))
show_svg()
```


![svg](README_files/README_48_0.svg)


## 8. Demographic and clinical characteristics of cohort


```bash
%%bash
let "CPU_AVAIL = $(nproc) - 1"
echo "Nr of CPUs available: ${CPU_AVAIL}"

export REPORT_DATE=$(date +%Y-%m-%d)
my_notebook='./notebooks/07_MOT_characteristics-of-cohort.ipynb'

time jupyter nbconvert \
    --ExecutePreprocessor.kernel_name='tb_microbiome' \
    --execute \
    --allow-errors \
    --clear-output \
    --ExecutePreprocessor.timeout=-1 \
    ${my_notebook}

time jupyter nbconvert \
    --to html \
    --output-dir='./reports' \
    --output=${REPORT_DATE}_$(echo ${my_notebook} | cut -d'/' -f3) \
    ${my_notebook}
```

    Nr of CPUs available: 7


    [NbConvertApp] WARNING | Config option `template_path` not recognized by `NotebookExporter`.
    [NbConvertApp] Converting notebook ./notebooks/07_MOT_characteristics-of-cohort.ipynb to notebook
    [NbConvertApp] Executing notebook with kernel: tb_microbiome
    [NbConvertApp] Writing 226837 bytes to ./notebooks/07_MOT_characteristics-of-cohort.ipynb
    
    real	0m15.935s
    user	0m7.069s
    sys	0m1.489s
    [NbConvertApp] Converting notebook ./notebooks/07_MOT_characteristics-of-cohort.ipynb to html
    [NbConvertApp] Writing 564162 bytes to ./reports/2020-07-16_07_MOT_characteristics-of-cohort.ipynb.html
    
    real	0m1.495s
    user	0m1.277s
    sys	0m0.216s


## 9. Taxonomic summaries


```bash
%%bash
let "CPU_AVAIL = $(nproc) - 1"
echo "Nr of CPUs available: ${CPU_AVAIL}"

export REPORT_DATE=$(date +%Y-%m-%d)
my_notebook='./notebooks/08_MOT_taxonomic-summaries.ipynb'

time jupyter nbconvert \
    --ExecutePreprocessor.kernel_name='tb_microbiome' \
    --execute \
    --allow-errors \
    --clear-output \
    --ExecutePreprocessor.timeout=-1 \
    ${my_notebook}

time jupyter nbconvert \
    --to html \
    --output-dir='./reports' \
    --output=${REPORT_DATE}_$(echo ${my_notebook} | cut -d'/' -f3) \
    ${my_notebook}
```

    Nr of CPUs available: 7


    [NbConvertApp] WARNING | Config option `template_path` not recognized by `NotebookExporter`.
    [NbConvertApp] Converting notebook ./notebooks/08_MOT_taxonomic-summaries.ipynb to notebook
    [NbConvertApp] Executing notebook with kernel: tb_microbiome
    [NbConvertApp] Writing 960230 bytes to ./notebooks/08_MOT_taxonomic-summaries.ipynb
    
    real	6m42.465s
    user	6m26.338s
    sys	0m20.235s
    [NbConvertApp] Converting notebook ./notebooks/08_MOT_taxonomic-summaries.ipynb to html
    [NbConvertApp] Writing 1437023 bytes to ./reports/2020-07-16_08_MOT_taxonomic-summaries.ipynb.html
    
    real	0m1.726s
    user	0m1.482s
    sys	0m0.243s



```python
from IPython.display import SVG, display
def show_svg():
    display(SVG('./reports/figures/taxonomic_abundances_panel.svg'))
show_svg()
```


![svg](README_files/README_53_0.svg)


## 10. Procrustes analysis to evaluate effect of rarefaction on Aitchison distances


```bash
%%bash
let "CPU_AVAIL = $(nproc) - 1"
echo "Nr of CPUs available: ${CPU_AVAIL}"

export REPORT_DATE=$(date +%Y-%m-%d)
my_notebook='./notebooks/09_MOT_rarefied-vs-nonrarefied-coda.ipynb'

time jupyter nbconvert \
    --ExecutePreprocessor.kernel_name='tb_microbiome' \
    --execute \
    --allow-errors \
    --clear-output \
    --ExecutePreprocessor.timeout=-1 \
    ${my_notebook}

time jupyter nbconvert \
    --to html \
    --output-dir='./reports' \
    --output=${REPORT_DATE}_$(echo ${my_notebook} | cut -d'/' -f3) \
    ${my_notebook}
```

    Nr of CPUs available: 7


    [NbConvertApp] WARNING | Config option `template_path` not recognized by `NotebookExporter`.
    [NbConvertApp] Converting notebook ./notebooks/09_MOT_rarefied-vs-nonrarefied-coda.ipynb to notebook
    [NbConvertApp] Executing notebook with kernel: tb_microbiome
    [NbConvertApp] Writing 561407 bytes to ./notebooks/09_MOT_rarefied-vs-nonrarefied-coda.ipynb
    
    real	4m42.114s
    user	7m30.580s
    sys	0m32.841s
    [NbConvertApp] Converting notebook ./notebooks/09_MOT_rarefied-vs-nonrarefied-coda.ipynb to html
    [NbConvertApp] Writing 898415 bytes to ./reports/2020-07-16_09_MOT_rarefied-vs-nonrarefied-coda.ipynb.html
    
    real	0m1.515s
    user	0m1.262s
    sys	0m0.251s



```python
from IPython.display import SVG, display
def show_svg():
    display(SVG('./reports/figures/16S-AS_norarefied-vs-rarefied-clr-ordinations.svg'))
show_svg()
```


![svg](README_files/README_56_0.svg)


## 11. Inference of genus-level microbial interactions


```bash
%%bash
let "CPU_AVAIL = $(nproc) - 1"
echo "Nr of CPUs available: ${CPU_AVAIL}"

export REPORT_DATE=$(date +%Y-%m-%d)
my_notebook='./notebooks/10_MOT_inference-of-genus-genus-interactions.ipynb'

time jupyter nbconvert \
    --ExecutePreprocessor.kernel_name='tb_microbiome' \
    --execute \
    --allow-errors \
    --clear-output \
    --ExecutePreprocessor.timeout=-1 \
    ${my_notebook}

time jupyter nbconvert \
    --to html \
    --output-dir='./reports' \
    --output=${REPORT_DATE}_$(echo ${my_notebook} | cut -d'/' -f3) \
    ${my_notebook}
```

    Nr of CPUs available: 7


    [NbConvertApp] WARNING | Config option `template_path` not recognized by `NotebookExporter`.
    [NbConvertApp] Converting notebook ./notebooks/10_MOT_inference-of-genus-genus-interactions.ipynb to notebook
    [NbConvertApp] Executing notebook with kernel: tb_microbiome
    [NbConvertApp] Writing 3575524 bytes to ./notebooks/10_MOT_inference-of-genus-genus-interactions.ipynb
    
    real	2m37.725s
    user	3m56.818s
    sys	0m44.022s
    [NbConvertApp] Converting notebook ./notebooks/10_MOT_inference-of-genus-genus-interactions.ipynb to html
    [NbConvertApp] Writing 3996050 bytes to ./reports/2020-07-16_10_MOT_inference-of-genus-genus-interactions.ipynb.html
    
    real	0m1.942s
    user	0m1.657s
    sys	0m0.280s



```python
from IPython.display import SVG, display
def show_svg():
    display(SVG('./reports/figures/asv16s_biplot_network_interactions.svg'))
show_svg()
```


![svg](README_files/README_59_0.svg)


## 12.  Inference of species-level microbial interactions


```bash
%%bash
let "CPU_AVAIL = $(nproc) - 1"
echo "Nr of CPUs available: ${CPU_AVAIL}"

export REPORT_DATE=$(date +%Y-%m-%d)
my_notebook='./notebooks/11_MOT_inference-of-spp-spp-interactions.ipynb'

time jupyter nbconvert \
    --ExecutePreprocessor.kernel_name='tb_microbiome' \
    --execute \
    --allow-errors \
    --clear-output \
    --ExecutePreprocessor.timeout=-1 \
    ${my_notebook}

time jupyter nbconvert \
    --to html \
    --output-dir='./reports' \
    --output=${REPORT_DATE}_$(echo ${my_notebook} | cut -d'/' -f3) \
    ${my_notebook}
```

    Nr of CPUs available: 7


    [NbConvertApp] WARNING | Config option `template_path` not recognized by `NotebookExporter`.
    [NbConvertApp] Converting notebook ./notebooks/11_MOT_inference-of-spp-spp-interactions.ipynb to notebook
    [NbConvertApp] Executing notebook with kernel: tb_microbiome
    [NbConvertApp] Writing 1023731 bytes to ./notebooks/11_MOT_inference-of-spp-spp-interactions.ipynb
    
    real	0m51.957s
    user	1m58.655s
    sys	0m29.007s
    [NbConvertApp] Converting notebook ./notebooks/11_MOT_inference-of-spp-spp-interactions.ipynb to html
    [NbConvertApp] Writing 1378283 bytes to ./reports/2020-07-17_11_MOT_inference-of-spp-spp-interactions.ipynb.html
    
    real	0m1.930s
    user	0m1.405s
    sys	0m0.248s



```python
from IPython.display import SVG, display
def show_svg():
    display(SVG('./reports/figures/wms_spp_network.svg'))
show_svg()
```


![svg](README_files/README_62_0.svg)


## 13. Testing associations with Faith's phylogenetic diversity


```bash
%%bash
let "CPU_AVAIL = $(nproc) - 1"
echo "Nr of CPUs available: ${CPU_AVAIL}"

export REPORT_DATE=$(date +%Y-%m-%d)
my_notebook='./notebooks/12_MOT_alpha-diversity-and-clinical-features.ipynb'

time jupyter nbconvert \
    --ExecutePreprocessor.kernel_name='tb_microbiome' \
    --execute \
    --allow-errors \
    --clear-output \
    --ExecutePreprocessor.timeout=-1 \
    ${my_notebook}

time jupyter nbconvert \
    --to html \
    --output-dir='./reports' \
    --output=${REPORT_DATE}_$(echo ${my_notebook} | cut -d'/' -f3) \
    ${my_notebook}
```

    Nr of CPUs available: 7


    [NbConvertApp] WARNING | Config option `template_path` not recognized by `NotebookExporter`.
    [NbConvertApp] Converting notebook ./notebooks/12_MOT_alpha-diversity-and-clinical-features.ipynb to notebook
    [NbConvertApp] Executing notebook with kernel: tb_microbiome
    [NbConvertApp] Writing 1069467 bytes to ./notebooks/12_MOT_alpha-diversity-and-clinical-features.ipynb
    
    real	2m1.441s
    user	2m8.117s
    sys	0m12.500s
    [NbConvertApp] Converting notebook ./notebooks/12_MOT_alpha-diversity-and-clinical-features.ipynb to html
    [NbConvertApp] Writing 1513016 bytes to ./reports/2020-07-20_12_MOT_alpha-diversity-and-clinical-features.ipynb.html
    
    real	0m2.076s
    user	0m1.579s
    sys	0m0.240s



```python
from IPython.display import SVG, display
def show_svg():
    display(SVG('./reports/figures/asv16s_faithpd_ancova_cxrs.svg'))
show_svg()

```


![svg](README_files/README_65_0.svg)


## 14. Testing associations with compositional variation


```bash
%%bash
let "CPU_AVAIL = $(nproc) - 1"
echo "Nr of CPUs available: ${CPU_AVAIL}"

export REPORT_DATE=$(date +%Y-%m-%d)
my_notebook='./notebooks/13_MOT_beta-diversity-and-clinical-features.ipynb'

time jupyter nbconvert \
    --ExecutePreprocessor.kernel_name='tb_microbiome' \
    --execute \
    --allow-errors \
    --clear-output \
    --ExecutePreprocessor.timeout=-1 \
    ${my_notebook}

time jupyter nbconvert \
    --to html \
    --output-dir='./reports' \
    --output=${REPORT_DATE}_$(echo ${my_notebook} | cut -d'/' -f3) \
    ${my_notebook}
```

    Nr of CPUs available: 7


    [NbConvertApp] WARNING | Config option `template_path` not recognized by `NotebookExporter`.
    [NbConvertApp] Converting notebook ./notebooks/13_MOT_beta-diversity-and-clinical-features.ipynb to notebook
    [NbConvertApp] Executing notebook with kernel: tb_microbiome
    [NbConvertApp] Writing 1346426 bytes to ./notebooks/13_MOT_beta-diversity-and-clinical-features.ipynb
    
    real	4m25.277s
    user	15m0.346s
    sys	3m34.408s
    [NbConvertApp] Converting notebook ./notebooks/13_MOT_beta-diversity-and-clinical-features.ipynb to html
    [NbConvertApp] Writing 1763112 bytes to ./reports/2020-07-20_13_MOT_beta-diversity-and-clinical-features.ipynb.html
    
    real	0m1.779s
    user	0m1.498s
    sys	0m0.277s



```python
from IPython.display import SVG, display
def show_svg():
    display(SVG('./reports/figures/as16s_betadiv_biplot_interactions_permanova.svg'))
show_svg()
```


![svg](README_files/README_68_0.svg)

