#!/bin/bash -O extglob
# author: Monica R. Ticlla
# contact: mticlla@gmail.com

# raw_get_16s_scicore_fastqs.sh: script to create links to raw fastq files of 
#                                the 16S TB microbiome project located at sciCORE

# Global variables
PROGNAME=$(basename $0)
# File with mapping of IDs
mapping_ids_file=${GROUP}/raw_data/tb_sputum_microbiome/internal_to_external_ids_master.tsv
# FastQ files from sputum-dna collected in TZ from TB cases and controls, and also
# FastQ files from sputum-dna collected in CH from TB cases and controls
parent_dir_1=${GROUP}/raw_data/tb_sputum_microbiome/12_02_2016_16S/data
# FastQ files from sputum-dna collected in TZ from TB cases and controls
parent_dir_2=${GROUP}/raw_data/tb_sputum_microbiome/04_07_2016_16SV3V4/data
# FastQ files from sputum-dna collected in TZ from TB cases
parent_dir_3=${GROUP}/raw_data/tb_sputum_microbiome/07_10_2016_16SV3V4/data
# FastQ files from sputum-dna collected in CH and Nasopharyngeal swabs from TZ
parent_dir_4=${GROUP}/raw_data/tb_sputum_microbiome/14_12_2016/data

# Functions
usage () {
    echo "$PROGNAME: usage: $PROGNAME [-d directory | -g group]
    Script to create soft links to raw fastq files of the 
    16S-TB-microbiome project located at sciCORE
    "
    echo "Options:
        -d, --directory    output directory where soft links will be created.
        -g, --group [all | tz_all | tz_cc | tz_spt | tz_spt_cc | tz_spt_cases] "
    return
}

create_fastq_manifest () {
    local samples_list_path=${1}
    local fastq_dir_path=${2}
    local label=${3}
    echo -e "sample-id\tforward-absolute-filepath\treverse-absolute-filepath">${directory}/fastq_manifest_${label}.tzv
    for sample in $(cat ${samples_list_path} | cut -f1); do
        r1_path=$(ls ${fastq_dir_path}/${sample}*_R1_*.fastq.gz)
        r2_path=$(ls ${fastq_dir_path}/${sample}*_R1_*.fastq.gz)
        echo -e "${sample}\t${r1_path}\t${r2_path}" >>${directory}/fastq_manifest_${label}.tzv
    done
#     paste <(cat ${samples_list_path} | cut -f1) \
#           <(ls ${fastq_dir_path}/*_R1_*.fastq.gz | grep -f ${samples_list_path}) \
#           <(ls ${fastq_dir_path}/*_R2_*.fastq.gz | grep -f ${samples_list_path}) \
#           >>${directory}/fastq_manifest_${label}.tzv
}

# Create hash table for mapping of IDs
declare -A IDS
declare -A IDStoSpecimenID
declare -A IDStoHostID
while IFS= read -r line; do
   key="$(echo $line | cut -f1 -d' ')"
   value="$(echo $line | cut -f2 -d' ')"
   host="$(echo $line | cut -f4 -d' ')"
   specimen="$(echo $line | cut -f6 -d' ')"
   IDS["$key"]="$value"
   IDStoSpecimenID["$key"]="$specimen"
   IDStoHostID["$key"]="$host"
done <<< "$(tail -n+2 $mapping_ids_file)"


# Process command line options
group=
directory=

if [[ -z "$1" ]]
then
    usage >&2
    exit 1
fi

while [[ -n $1 ]];do
    case $1 in
        -d | --directory)    shift
                             directory=$1
                             ;;
        -g | --group)        shift
                             group=$1
                             ;;
        -h | --help)         usage
                             exit
                             ;;
        *)                   usage >&2
                             exit 1
                             ;;
    esac
    shift
done

# Main program

# Depending on the specified group of samples
files_1=
files_2=
files_3=
files_4=
case $group in
    all)          files_1=($(ls ${parent_dir_1}/{LF,8{0,3}}+([0-9])_L1* | cut -d '/' -f 10))
                  files_2=($(ls ${parent_dir_2}))
                  files_3=($(ls ${parent_dir_3}))
                  files_4=($(ls ${parent_dir_4}))
                  ;;
    tz_all)       files_1=($(ls ${parent_dir_1}/8{0,3}+([0-9])_L1* | cut -d '/' -f 10))
                  files_2=($(ls ${parent_dir_2}/8{0,3}+([0-9])_L1* | cut -d '/' -f 10))
                  files_3=($(ls ${parent_dir_3}/8{0,3}+([0-9])_L1* | cut -d '/' -f 10))
                  files_4=($(ls ${parent_dir_4}/S* | cut -d '/' -f 10))
                  ;;
    tz_cc)        files_1=($(ls ${parent_dir_1}/8{0,3}+([0-9])_L1* | cut -d '/' -f 10))
                  files_2=($(ls ${parent_dir_2}/8{0,3}+([0-9])_L1* | cut -d '/' -f 10))
                  files_4=($(ls ${parent_dir_4}/S* | cut -d '/' -f 10))
                  ;;      
    tz_spt)       files_1=($(ls ${parent_dir_1}/8{0,3}+([0-9])_L1* | cut -d '/' -f 10))
                  files_2=($(ls ${parent_dir_2}/8{0,3}+([0-9])_L1* | cut -d '/' -f 10))
                  files_3=($(ls ${parent_dir_3}/8{0,3}+([0-9])_L1* | cut -d '/' -f 10))
                  ;;
    tz_spt_cc)    files_1=($(ls ${parent_dir_1}/8{0,3}+([0-9])_L1* | cut -d '/' -f 10))
                  files_2=($(ls ${parent_dir_2}/8{0,3}+([0-9])_L1* | cut -d '/' -f 10))
                  ;;
    tz_spt_cases) files_1=($(ls ${parent_dir_1}/80+([0-9])_L1* | cut -d '/' -f 10))
                  files_2=($(ls ${parent_dir_2}/80+([0-9])_L1* | cut -d '/' -f 10))
                  files_3=($(ls ${parent_dir_3}/80+([0-9])_L1* | cut -d '/' -f 10))
                  ;;
    *)            usage >&2
                  echo "--group [all | tz_all | tz_cc | tz_spt | tz_spt_cc | tz_spt_cases]"
                  echo "Provide a valid option for --group!"
                  exit 1
                  ;;
esac

#Softs links to all fastq files are created in this folder
fastq_dir=$directory/fastq

if [ -d ${fastq_dir} ]; then 
    rm -r ${fastq_dir}
fi

mkdir ${fastq_dir}
fastq_dir_abs_path=$(readlink -f ${fastq_dir})
if [[ ! -z "$files_1" ]]
then
    run='run1'
    #Get number of files in ${files_1}
    echo -e "Number of files from run1"
    echo ${#files_1[@]}
    for file in ${files_1[@]};do
        target=$( echo ${file} | sed 's/001_.\{12\}./001./g; s/_L1/.1_L1/g')
        id="$(echo $target | cut -d'_' -f1)"
        suffix="$(echo $target | cut -d'_' -f2,3,4 --output-delimiter '_')"
        #ln -s ${parent_dir_1}/${file} ${fastq_dir}/${target}
        ln -s ${parent_dir_1}/${file} ${fastq_dir}/${IDS[$id]}_${suffix}
        echo -e "${IDS[$id]}\t${IDStoSpecimenID[$id]}\t${IDStoHostID[$id]}\t${run}"
    done | sort -u > ${directory}/sample_names_run1.txt
    #basename -a $(ls ${fastq_dir}/*.1_L1_R1*) | cut -d "_" -f 1 > ${directory}/sample_names_run1.txt
    create_fastq_manifest ${directory}/sample_names_run1.txt ${fastq_dir_abs_path} "$run"
fi

if [[ ! -z "$files_2" ]]
then
    run='run2'
    echo -e "Number of files from run2"
    echo ${#files_2[@]}
    for file in ${files_2[@]};do
        target=$( echo ${file} | sed 's/001_.\{12\}./001./g; s/_L1/.2_L1/g')
        id="$(echo $target | cut -d'_' -f1)"
        suffix="$(echo $target | cut -d'_' -f2,3,4 --output-delimiter '_')"
        #ln -s ${parent_dir_2}/${file} ${fastq_dir}/${target}
        ln -s ${parent_dir_2}/${file} ${fastq_dir}/${IDS[$id]}_${suffix}
        echo -e "${IDS[$id]}\t${IDStoSpecimenID[$id]}\t${IDStoHostID[$id]}\t${run}"
    done | sort -u > ${directory}/sample_names_run2.txt
    #basename -a $(ls ${fastq_dir}/*.2_L1_R1*) | cut -d "_" -f 1 > ${directory}/sample_names_run2.txt
    create_fastq_manifest ${directory}/sample_names_run2.txt ${fastq_dir_abs_path} "$run"
fi

if [[ ! -z "$files_3" ]]
then
    run='run3'
    echo -e "Number of files from run3"
    echo ${#files_3[@]}
    for file in ${files_3[@]};do
        target=$( echo ${file} | sed 's/001_.\{12\}./001./g; s/_L1/.3_L1/g')
        id="$(echo $target | cut -d'_' -f1)"
        suffix="$(echo $target | cut -d'_' -f2,3,4 --output-delimiter '_')"
        #ln -s ${parent_dir_3}/${file} ${fastq_dir}/${target}
        ln -s ${parent_dir_3}/${file} ${fastq_dir}/${IDS[$id]}_${suffix}
        echo -e "${IDS[$id]}\t${IDStoSpecimenID[$id]}\t${IDStoHostID[$id]}\t${run}"
    done | sort -u > ${directory}/sample_names_run3.txt
    #basename -a $(ls ${fastq_dir}/*.3_L1_R1*) | cut -d "_" -f 1 > ${directory}/sample_names_run3.txt
    create_fastq_manifest ${directory}/sample_names_run3.txt ${fastq_dir_abs_path} "$run"
fi

if [[ ! -z "$files_4" ]]
then
    run='run4'
    echo -e "Number of files from run4"
    echo ${#files_4[@]}
    for file in ${files_4[@]};do
        target=$( echo ${file} | sed 's/001_.\{12\}./001./g; s/_L1/.4_L1/g; s/S_/S/g')
        id="$(echo $target | cut -d'_' -f1)"
        suffix="$(echo $target | cut -d'_' -f2,3,4 --output-delimiter '_')"
        #ln -s ${parent_dir_4}/${file} ${fastq_dir}/${target}
        ln -s ${parent_dir_4}/${file} ${fastq_dir}/${IDS[$id]}_${suffix}
        echo -e "${IDS[$id]}\t${IDStoSpecimenID[$id]}\t${IDStoHostID[$id]}\t${run}"
    done | sort -u > ${directory}/sample_names_run4.txt
    #basename -a $(ls ${fastq_dir}/*.4_L1_R1*) | cut -d "_" -f 1 > ${directory}/sample_names_run4.txt
    create_fastq_manifest ${directory}/sample_names_run4.txt ${fastq_dir_abs_path} "$run"
fi

# Create the fastq_manifest.tsv
# all samples
ls ${fastq_dir} | cut -d "_" -f 1 | sort -u > ${directory}/sample_names.txt

echo -e "sample-id\tSpecimenID\tSubjectID\tseq_run" >${directory}/sample_ids_mapping.txt
cat ${directory}/sample_names_run*.txt >>${directory}/sample_ids_mapping.txt

echo -e "sample-id\tforward-absolute-filepath\treverse-absolute-filepath">${directory}/fastq_manifest.tzv
paste <(cat ${directory}/sample_names.txt | cut -f1) \
      <(ls ${fastq_dir_abs_path}/*_R1_*.fastq.gz | grep -f ${directory}/sample_names.txt) \
      <(ls ${fastq_dir_abs_path}/*_R2_*.fastq.gz | grep -f ${directory}/sample_names.txt) \
      >>${directory}/fastq_manifest.tzv

echo "Number of links created in ${fastq_dir}:"
ls ${fastq_dir} | wc -l
echo "Number of samples:"
nr_lines=$(wc -l ${directory}/sample_names.txt | cut -d' ' -f1)
echo "${nr_lines}"