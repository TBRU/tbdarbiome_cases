#!/bin/bash
# author: Monica R. Ticlla
# contact: mticlla@gmail.com

# raw_get_wms_scicore_fastqs.sh: script to create links to raw fastq files of 
#                                whole metagenome shotgun (WMS) sequencing data 
#                                generated for the TB microbiome project and 
#                                located at sciCORE

# Global variables
# -------------------------------
PROGNAME=$(basename $0)
# Files with mapping of IDS
dbsse_mapping_ids_file=${GROUP}/raw_data/tb_sputum_microbiome/dbsse_internal_to_external_ids.tsv
fisabio_mapping_ids_file=${GROUP}/raw_data/tb_sputum_microbiome/fisabio_internal_to_external_ids.tsv
# Directories with raw fastq files
dbsse_dir=${GROUP}/raw_data/tb_sputum_microbiome/19_01_2019_dbsse
fisabio_dir=${GROUP}/raw_data/tb_sputum_microbiome/01_12_2017

#19_01_2019_dbsse/samples_dbsse_scicorepaths.txt/

# Functions
# --------------------------------
usage () {
    echo "$PROGNAME: usage: $PROGNAME [-d directory | -s dataset | -g group]
    script to create links to raw fastq files of 
    whole metagenome shotgun (WMS) sequencing data 
    generated for the TB microbiome project and 
    located at sciCORE.
    "
    echo "Options:
        -d, --directory    output directory where soft links will be created.
        -s, --dataset      [dbsse | fisabio]
        -g, --group [all | tz_all | tz_spt | tz_spt_cases] "
    return
}

# Process command line options
group=
directory=
dataset=
if [[ -z "$1" ]]
then
    usage >&2
    exit 1
fi

while [[ -n $1 ]];do
    case $1 in
        -d | --directory)    shift
                             directory=$1
                             ;;
        -s | --dataset)      shift
                             dataset=$1
                             ;;
        -g | --group)        shift
                             group=$1
                             ;;
        -h | --help)         usage
                             exit
                             ;;
        *)                   usage >&2
                             exit 1
                             ;;
    esac
    shift
done

# Main program
# ---------------------------------------
# Create hash table for mapping of IDs
declare -A IDS
declare -A IDStoSpecimenID
declare -A IDStoHostID
case $dataset in
    dbsse)    while IFS= read -r line; do
                  key="$(echo $line | cut -f1 -d' ')"
                  value="$(echo $line | cut -f2 -d' ')"
                  host="$(echo $line | cut -f6 -d' ')"
                  specimen="$(echo $line | cut -f4 -d' ')"
                  IDS["$key"]="$value"
                  IDStoSpecimenID["$key"]="$specimen"
                  IDStoHostID["$key"]="$host"
              done <<< "$(tail -n+2 $dbsse_mapping_ids_file)"
              ;;
    fisabio)  while IFS= read -r line; do
                  key="$(echo $line | cut -f1 -d' ')"
                  value="$(echo $line | cut -f2 -d' ')"
                  host="$(echo $line | cut -f6 -d' ')"
                  specimen="$(echo $line | cut -f4 -d' ')"
                  IDS["$key"]="$value"
                  IDStoSpecimenID["$key"]="$specimen"
                  IDStoHostID["$key"]="$host"
              done <<< "$(tail -n+2 $fisabio_mapping_ids_file)"
              ;;
    *)        usage >&2
              echo "--dataset      [dbsse | fisabio]"
              echo "Provide a valid option for --dataset!"
              exit 1
              ;;
esac

# Create array with samples IDs for which to create links to their fastq files
samples=
case $group in 
    all)              samples=(${!IDS[@]})
                      ;;
    tz_all)           if [ "$dataset" = "dbsse" ]; then
                          samples=($(egrep "^8|^S|^N|^P" ${dbsse_dir}/samples_dbsse.txt))
                      elif [ "$dataset" = "fisabio" ]; then
                          samples=($(cat ${fisabio_dir}/fisabio_sample_names.txt))
                      fi
                      ;;
    tz_spt)           if [ "$dataset" = "dbsse" ]; then
                          samples=($(egrep "^8|^N|^P" ${dbsse_dir}/samples_dbsse.txt))
                      elif [ "$dataset" = "fisabio" ]; then
                          samples=($(cat ${fisabio_dir}/fisabio_sample_names.txt))
                      fi
                      ;;
    tz_spt_cases)     if [ "$dataset" = "dbsse" ]; then
                          samples=($(egrep "^80|^N|^P" ${dbsse_dir}/samples_dbsse.txt))
                      elif [ "$dataset" = "fisabio" ]; then
                          samples=($(egrep "^80|^N|^P" ${fisabio_dir}/fisabio_sample_names.txt))
                      fi
                      ;;
    *)                usage >&2
                      echo "--group [all | tz_all | tz_spt | tz_spt_cases]"
                      echo "Provide a valid option for --group!"
                      exit 1
                      ;;
esac
echo "Nr. of samples for which to retrieve fastq filepaths: ${#samples[@]}"

# File with mapping IDs
echo -e "sample-id\tSpecimenID\tSubjectID" >${directory}/sample_ids_mapping.txt
# For each sample:
# Create soft links to fastq paths
fastq_dir=${directory}/fastq
[ ! -d ${fastq_dir} ] && mkdir -p ${fastq_dir}
if [ "$dataset" = "dbsse" ]; then
    for sample in ${samples[@]}; do
        public_id="${IDS[$sample]}";
        echo -e "${public_id}\t${IDStoSpecimenID[$sample]}\t${IDStoHostID[$sample]}">>${directory}/sample_ids_mapping.txt;
        echo ${public_id}
        #  - Links to fastq files for read 1
        for filepath in $(grep -E "[1-8]{1,1}\_${sample}\_[ATCG]{8,8}\_.*R[1]{1,1}.*fastq\.gz$" ${dbsse_dir}/samples_dbsse_scicorepaths.txt); do
            fname=$(echo ${filepath} | rev | cut -d '/' -f1 | rev);
            run_lane=$(echo ${fname} | cut -d '_' -f4,5);
            ln -s ${filepath} ${fastq_dir}/${public_id}-${run_lane}-R1.fastq.gz;
        done
        #  - Links to fastq files for read 2
        for filepath in $(grep -E "[1-8]{1,1}\_${sample}\_[ATCG]{8,8}\_.*R[2]{1,1}.*fastq\.gz$" ${dbsse_dir}/samples_dbsse_scicorepaths.txt); do
            fname=$(echo ${filepath} | rev | cut -d '/' -f1 | rev);
            run_lane=$(echo ${fname} | cut -d '_' -f4,5);
            ln -s ${filepath} ${fastq_dir}/${public_id}-${run_lane}-R2.fastq.gz;
        done
    done > ${directory}/sample_names.txt;
elif [ "$dataset" = "fisabio" ]; then
    run_lane="LKF16085_1";
    for sample in ${samples[@]}; do
        public_id=${IDS[$sample]};
        echo -e "${public_id}\t${IDStoSpecimenID[$sample]}\t${IDStoHostID[$sample]}">>${directory}/sample_ids_mapping.txt;
        echo ${public_id}
        # filepaths to fastq files
        filepath_r1=$(ls ${fisabio_dir}/LKF16-085/fastq/${sample}_R1.fastq.gz);
        filepath_r2=$(ls ${fisabio_dir}/LKF16-085/fastq/${sample}_R2.fastq.gz);
        # links to fastq files
        ln -s ${filepath_r1} ${fastq_dir}/${public_id}-${run_lane}-R1.fastq.gz;
        ln -s ${filepath_r2} ${fastq_dir}/${public_id}-${run_lane}-R2.fastq.gz;
    done > ${directory}/sample_names.txt;
fi

echo "Number of links created in ${fastq_dir}: $(ls ${fastq_dir} | wc -l)"
echo "Number of samples: $(wc -l ${directory}/sample_names.txt | cut -d' ' -f1)"
